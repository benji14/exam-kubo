const fs = require('fs');
const async = require('async');

let listData = []


function saveData() {
    let data = JSON.stringify(listData)
    fs.writeFile('db/fuzzy-search.txt', data, (err) => {
        if (err) throw new Error('Error to save data', err)
        console.log('Usuario agregado')
    })

}


function loadData(callback) {
    try {
        fs.readFile('./db/fuzzy-search.txt', 'utf8', (err, content) => {
            if (err) throw new Error('Error to read data', err);
            if (content) {
                listData = JSON.parse(content)
                callback()
            } else {
                listData = [];
                callback()
            }
        });
    } catch (error) {
        listData = []
        callback(err);
    }
}

function create(name) {
    loadData((err) => {
        if (err) throw new Error('Error to load data', err);
        let aux = {
            name
        }

        listData.push(aux)
        saveData();
        return aux
    });

}

function getData(callback) {
    loadData((err) => {
        if (err) throw new Error('Error to load data', err);
        callback(listData);
    })
}

function getDataByName(name, callback) {
    let search = null
    loadData((err) => {
        if (err) throw new Error('Error to load data', err);
        async.each(listData, (person, call) => {
            if (person.name == name) {
                search = person.name
            }
            call()
        }, err => {
            callback(search)
        })
    })

}

module.exports = {
    create,
    getData,
    getDataByName
}