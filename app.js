// const argv = require('yargs').argv;

const argv = require('./config/index').argv;
const saveData = require('./logic');


let comando = argv._[0];

switch (comando) {
    case 'add':
        saveData.create(argv.name)
        break;
    case 'list':
        saveData.getData((data) => {
            console.log(data)
        })
        break;
    case 'search':
        saveData.getDataByName(argv.name, (data) => {
            if (data) {
                console.log({ name: data });
            } else {
                console.log('Sin coincidencias')
            }
        })
        break;
    default:
        console.log('El comando no es reconocido');
        break;
}