const argv = require('yargs')
    .command('add', 'Add a person', {
        name: {
            demand:true,
            alias: 'n',
            desc: 'Name person'
        }
    })
    .command('search', 'Search a person', {
        name: {
            demand:true,
            alias: 'n',
            desc: 'Name person'
        }
    })
    .help()
    .argv

module.exports = {
    argv
}